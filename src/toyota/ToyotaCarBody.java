package toyota;

import car_tools.CarBody;

public class ToyotaCarBody extends CarBody {
    public ToyotaCarBody(String color, String type, String producer, Double price){
        super(color, type, producer, price);
    }
}
