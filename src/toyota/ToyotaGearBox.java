package toyota;

import car_tools.GearBox;

public class ToyotaGearBox extends GearBox {
    public ToyotaGearBox(String type, Integer countSpeed, String producer, Double price){
        super(type, countSpeed, producer, price);
    }
}
