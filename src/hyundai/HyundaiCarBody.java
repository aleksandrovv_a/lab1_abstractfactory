package hyundai;

import car_tools.CarBody;

public class HyundaiCarBody extends CarBody {
    public HyundaiCarBody(String color, String type, String producer, Double price){
        super(color, type, producer, price);
    }
}
