package hyundai;

import car_tools.Wheel;

public class HyundaiWheel extends Wheel {
    public HyundaiWheel(String model, Double diameter, String producer, Double price){
        super(model, diameter, producer, price);
    }
}
