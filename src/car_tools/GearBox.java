package car_tools;

abstract public class GearBox {
    String type;
    Integer countSpeed;
    String producer;
    Double price;

    public GearBox(String type, Integer countSpeed, String producer, Double price) {
        this.type = type;
        this.countSpeed = countSpeed;
        this.producer = producer;
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public Integer getCountSpeed() {
        return countSpeed;
    }

    public String getProducer() {
        return producer;
    }

    public Double getPrice() {
        return price;
    }
}
