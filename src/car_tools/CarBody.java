package car_tools;

abstract public class CarBody {

    String color;
    String type;
    String producer;
    Double price;

    public CarBody(String color, String type, String producer, Double price) {
        this.color = color;
        this.type = type;
        this.producer = producer;
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public String getType() {
        return type;
    }

    public String getProducer() {
        return producer;
    }

    public Double getPrice() {
        return price;
    }
}
