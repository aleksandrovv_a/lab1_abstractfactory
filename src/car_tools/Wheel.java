package car_tools;

abstract public class Wheel {

    String model;
    Double diameter;
    String producer;
    Double price;

    public Wheel(String model, Double diameter, String producer, Double price) {
        this.model = model;
        this.diameter = diameter;
        this.producer = producer;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public Double getDiameter() {
        return diameter;
    }

    public String getProducer() {
        return producer;
    }

    public Double getPrice() {
        return price;
    }
}
