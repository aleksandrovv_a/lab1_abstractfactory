package factory;

import car_tools.CarBody;
import car_tools.GearBox;
import car_tools.Wheel;
import hyundai.HyundaiCarBody;
import hyundai.HyundaiGearBox;
import hyundai.HyundaiWheel;


public class HyundaiFactory implements CarsFactory {
    @Override
    public Wheel createWheel() {
        return new HyundaiWheel("Michelin Energy Saver A/S", 18.6, "Michelin", 78.00);
    }

    @Override
    public CarBody createCarBody() {
        return new HyundaiCarBody("Gray", "Universal", "Hyundai Mobis", 419.01);
    }

    @Override
    public GearBox createGearBox() {
        return new HyundaiGearBox("Automatic", 7, "Hyundai Mobis", 259.00);
    }
}
