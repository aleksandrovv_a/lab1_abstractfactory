package factory;

import bmw.BmwCarBody;
import bmw.BmwGearBox;
import bmw.BmwWheel;
import car_tools.CarBody;
import car_tools.GearBox;
import car_tools.Wheel;


public class BmwFactory implements CarsFactory {
    @Override
    public Wheel createWheel() {
        return new BmwWheel("Bridgestone Potenz", 17.2, "Bridgestone", 90.00);
    }

    @Override
    public CarBody createCarBody() {
        return new BmwCarBody("Gold", "SUV", "Magna Steyr", 326.89);
    }

    @Override
    public GearBox createGearBox() {
        return new BmwGearBox("Automated Manual", 7, "ZF Friedrichshafen AG", 516.08);
    }
}
