package factory;

import car_tools.CarBody;
import car_tools.GearBox;
import car_tools.Wheel;
import toyota.ToyotaCarBody;
import toyota.ToyotaGearBox;
import toyota.ToyotaWheel;

public class ToyotaFactory implements CarsFactory {
    @Override
    public Wheel createWheel() {
        return new ToyotaWheel("Toyota OEM car_tools.Wheel", 15.7, "Toyota", 90.07);
    }

    @Override
    public CarBody createCarBody() {
        return new ToyotaCarBody("Black", "Sedan", "Magna International", 247.52);
    }

    @Override
    public GearBox createGearBox() {
        return new ToyotaGearBox("Manual", 5, "Aisin Seiki Co., Ltd.", 173.87);
    }
}
