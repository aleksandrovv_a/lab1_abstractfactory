package factory;

import car_tools.CarBody;
import car_tools.GearBox;
import car_tools.Wheel;

public interface CarsFactory {

    Wheel createWheel();

    CarBody createCarBody();

    GearBox createGearBox();


}
