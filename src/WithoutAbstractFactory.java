import bmw.BmwCarBody;
import bmw.BmwGearBox;
import bmw.BmwWheel;
import car_tools.CarBody;
import car_tools.GearBox;
import car_tools.Wheel;
import factory.BmwFactory;
import factory.CarsFactory;
import factory.HyundaiFactory;
import factory.ToyotaFactory;
import hyundai.HyundaiCarBody;
import hyundai.HyundaiGearBox;
import hyundai.HyundaiWheel;
import toyota.ToyotaCarBody;
import toyota.ToyotaGearBox;
import toyota.ToyotaWheel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WithoutAbstractFactory {
    static HyundaiWheel hyundaiWheel;
    static HyundaiCarBody hyundaiCarBody;
    static HyundaiGearBox hyundaiGearBox;
    static ToyotaCarBody toyotaCarBody;
    static ToyotaGearBox toyotaGearBox;
    static ToyotaWheel toyotaWheel;
    static BmwCarBody bmwCarBody;
    static BmwGearBox bmwGearBox;
    static BmwWheel bmwWheel;

    public static void createHyundai() {
        hyundaiWheel = new HyundaiWheel("Michelin Energy Saver A/S", 18.6, "Michelin", 78.00);
        hyundaiCarBody = new HyundaiCarBody("Gray", "Universal", "Hyundai Mobis", 419.01);
        hyundaiGearBox = new HyundaiGearBox("Automatic", 7, "Hyundai Mobis", 259.00);
    }

    public static void createToyota(){
        toyotaWheel = new ToyotaWheel("Toyota OEM car_tools.Wheel", 15.7, "Toyota", 90.07);
        toyotaCarBody = new ToyotaCarBody("Black", "Sedan", "Magna International", 247.52);
        toyotaGearBox = new ToyotaGearBox("mann", 5, "Aisin Seiki Co., Ltd.", 173.87);
    }

    public static void createBmw(){
        bmwWheel = new BmwWheel("Bridgestone Potenz", 17.2, "Bridgestone", 90.00);
        bmwCarBody = new BmwCarBody("Gold", "SUV", "Magna Steyr", 326.89);
        bmwGearBox = new BmwGearBox("Automated Manual", 7, "ZF Friedrichshafen AG", 516.08);

    }

    public static void main(String[] args){


        JFrame frame = new JFrame("Car Factory Interface");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 600);

        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5, 5, 5, 5);

        JLabel label = new JLabel("Select a car manufacturer:");
        label.setFont(new Font("Arial", Font.BOLD, 16));
        constraints.gridx = 0;
        constraints.gridy = 0;
        panel.add(label, constraints);

        JComboBox<String> comboBox = new JComboBox<>(new String[]{"BMW", "Hyundai", "Toyota"});
        constraints.gridx = 1;
        constraints.gridy = 0;
        panel.add(comboBox, constraints);

        JButton button = new JButton("Choose car");
        button.setPreferredSize(new Dimension(120, 30)); // размер кнопки
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2; // ширина кнопки на две ячейки
        panel.add(button, constraints);

        JLabel resultLabel = new JLabel();
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        panel.add(resultLabel, constraints);


        JButton moreInfoButton = new JButton("More Info");
        moreInfoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String resultText = "<html><table border=\"1\">" +
                        "<tr bgcolor=\"#e3e3fe\"><th colspan=\"3\">Car Information</th></tr>" +
                        "<tr bgcolor=\"#e3e3fe\"><th>Компонент</th><th>Характеристика</th><th>Значение</th></tr>" +
                        "<tr bgcolor=\"#e3e3fe\"><th colspan=\"3\">BMW</th></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td>Колесо</td><td>Модель</td><td>" + bmwWheel.getModel() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Диаметр</td><td>" + bmwWheel.getDiameter() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Производитель</td><td>" + bmwWheel.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Цена</td><td>" + bmwWheel.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td>Кузов</td><td>Цвет</td><td>" + bmwCarBody.getColor() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Тип</td><td>" + bmwCarBody.getType() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Производитель</td><td>" + bmwCarBody.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Цена</td><td>" + bmwCarBody.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td>Коробка передач</td><td>Тип</td><td>" + bmwGearBox.getType() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Количество передач</td><td>" + bmwGearBox.getCountSpeed() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Производитель</td><td>" + bmwGearBox.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Цена</td><td>" + bmwGearBox.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#e3e3fe\"><th colspan=\"3\">Toyota</th></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td>Колесо</td><td>Модель</td><td>" + toyotaWheel.getModel() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Диаметр</td><td>" + toyotaWheel.getDiameter() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Производитель</td><td>" + toyotaWheel.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Цена</td><td>" + toyotaWheel.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td>Кузов</td><td>Цвет</td><td>" + toyotaCarBody.getColor() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Тип</td><td>" + toyotaCarBody.getType() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Производитель</td><td>" + toyotaCarBody.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Цена</td><td>" + toyotaCarBody.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td>Коробка передач</td><td>Тип</td><td>" + toyotaGearBox.getType() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Количество передач</td><td>" + toyotaGearBox.getCountSpeed() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Производитель</td><td>" + toyotaGearBox.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Цена</td><td>" + toyotaGearBox.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#e3e3fe\"><th colspan=\"3\">Hyundai</th></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td>Колесо</td><td>Модель</td><td>" + hyundaiWheel.getModel() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Диаметр</td><td>" + hyundaiWheel.getDiameter() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Производитель</td><td>" + hyundaiWheel.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#fbf0f0\"><td></td><td>Цена</td><td>" + hyundaiWheel.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td>Кузов</td><td>Цвет</td><td>" + hyundaiCarBody.getColor() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Тип</td><td>" + hyundaiCarBody.getType() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Производитель</td><td>" + hyundaiCarBody.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#d2e7fe\"><td></td><td>Цена</td><td>" + hyundaiCarBody.getPrice() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td>Коробка передач</td><td>Тип</td><td>" + hyundaiGearBox.getType() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Количество передач</td><td>" + hyundaiGearBox.getCountSpeed() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Производитель</td><td>" + hyundaiGearBox.getProducer() + "</td></tr>" +
                        "<tr bgcolor=\"#dcdcdc\"><td></td><td>Цена</td><td>" + hyundaiGearBox.getPrice() + "</td></tr>" +
                        "</table></html>";

                JLabel label = new JLabel(resultText);

                // Создаем панель с полосами прокрутки
                JPanel panel = new JPanel(new BorderLayout());
                panel.add(label);

                // Создаем прокручиваемую панель с содержимым
                JScrollPane scrollPane = new JScrollPane(panel);

                // Устанавливаем размеры прокручиваемой панели
                scrollPane.setPreferredSize(new Dimension(500, 600));

                // Отображаем окно с результатами
                JOptionPane.showMessageDialog(null, scrollPane);
            }

        });
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 2;
        panel.add(moreInfoButton, constraints);

        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose(); // Закрываем окно
            }
        });
        constraints.gridx = 0;
        constraints.gridy = 4;
        constraints.gridwidth = 2;
        panel.add(closeButton, constraints);

        frame.getContentPane().add(panel);
        frame.setLocationRelativeTo(null); // окно по центру экрана
        frame.setVisible(true);

        button.addActionListener(new ActionListener() {
                                     @Override
                                     public void actionPerformed(ActionEvent e) {
                                         String selectedManufacturer = (String) comboBox.getSelectedItem();
                                         if (selectedManufacturer != null) {

                                             switch (selectedManufacturer) {
                                                 case "BMW":
                                                     createBmw();
                                                     break;
                                                 case "Hyundai":
                                                     createHyundai();
                                                     break;
                                                 case "Toyota":
                                                     createToyota();
                                                     break;
                                             }

                                             if (selectedManufacturer.equals("BMW")) {
                                                 String resultText = "<html><table border=\"1\">" +
                                                         "<tr bgcolor=\"#e3e3fe\"><th>Компонент</th><th>Характеристика</th><th>Значение</th></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td>Колесо</td><td>Модель</td><td>" + bmwWheel.getModel() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Диаметр</td><td>" + bmwWheel.getDiameter() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Производитель</td><td>" + bmwWheel.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Цена</td><td>" + bmwWheel.getPrice() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td>Кузов</td><td>Цвет</td><td>" + bmwCarBody.getColor() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Тип</td><td>" + bmwCarBody.getType() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Производитель</td><td>" + bmwCarBody.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Цена</td><td>" + bmwCarBody.getPrice() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td>Коробка передач</td><td>Тип</td><td>" + bmwGearBox.getType() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Количество передач</td><td>" + bmwGearBox.getCountSpeed() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Производитель</td><td>" + bmwGearBox.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Цена</td><td>" + bmwGearBox.getPrice() + "</td></tr>" +
                                                         "</table></html>";
                                                 resultLabel.setText(resultText);
                                             }
                                             else if (selectedManufacturer.equals("Hyundai")) {
                                                 String resultText = "<html><table border=\"1\">" +
                                                         "<tr bgcolor=\"#e3e3fe\"><th>Компонент</th><th>Характеристика</th><th>Значение</th></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td>Колесо</td><td>Модель</td><td>" + hyundaiWheel.getModel() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Диаметр</td><td>" + hyundaiWheel.getDiameter() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Производитель</td><td>" + hyundaiWheel.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Цена</td><td>" + hyundaiWheel.getPrice() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td>Кузов</td><td>Цвет</td><td>" + hyundaiCarBody.getColor() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Тип</td><td>" + hyundaiCarBody.getType() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Производитель</td><td>" + hyundaiCarBody.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Цена</td><td>" + hyundaiCarBody.getPrice() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td>Коробка передач</td><td>Тип</td><td>" + hyundaiGearBox.getType() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Количество передач</td><td>" + hyundaiGearBox.getCountSpeed() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Производитель</td><td>" + hyundaiGearBox.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Цена</td><td>" + hyundaiGearBox.getPrice() + "</td></tr>" +
                                                         "</table></html>";
                                                 resultLabel.setText(resultText);
                                             }
                                             else if (selectedManufacturer.equals("Toyota")) {
                                                 String resultText = "<html><table border=\"1\">" +
                                                         "<tr bgcolor=\"#e3e3fe\"><th>Компонент</th><th>Характеристика</th><th>Значение</th></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td>Колесо</td><td>Модель</td><td>" + toyotaWheel.getModel() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Диаметр</td><td>" + toyotaWheel.getDiameter() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Производитель</td><td>" + toyotaWheel.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#fbf0f0\"><td></td><td>Цена</td><td>" + toyotaWheel.getPrice() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td>Кузов</td><td>Цвет</td><td>" + toyotaCarBody.getColor() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Тип</td><td>" + toyotaCarBody.getType() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Производитель</td><td>" + toyotaCarBody.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#d2e7fe\"><td></td><td>Цена</td><td>" + toyotaCarBody.getPrice() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td>Коробка передач</td><td>Тип</td><td>" + toyotaGearBox.getType() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Количество передач</td><td>" + toyotaGearBox.getCountSpeed() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Производитель</td><td>" + toyotaGearBox.getProducer() + "</td></tr>" +
                                                         "<tr bgcolor=\"#dcdcdc\"><td></td><td>Цена</td><td>" + toyotaGearBox.getPrice() + "</td></tr>" +
                                                         "</table></html>";
                                                 resultLabel.setText(resultText);
                                             }
                                         }
                                     }
                                 }
        );
    }
}
