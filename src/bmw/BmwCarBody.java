package bmw;

import car_tools.CarBody;

public class BmwCarBody extends CarBody {
    public BmwCarBody(String color, String type, String producer, Double price) {
        super(color, type, producer, price);
    }
}
