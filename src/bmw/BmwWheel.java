package bmw;

import car_tools.Wheel;

public class BmwWheel extends Wheel {

    public BmwWheel(String model, Double diameter, String producer, Double price) {
        super(model, diameter, producer, price);
    }
}
